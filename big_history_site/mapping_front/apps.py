from django.apps import AppConfig


class MappingFrontConfig(AppConfig):
    name = 'mapping_front'
