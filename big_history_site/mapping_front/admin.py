from django.contrib import admin

from .models import *

admin.site.register(BigHistoryUnit)
admin.site.register(CiricSpecific)
admin.site.register(CiricLearningArea)
admin.site.register(Mapping)
admin.site.register(CiricLearningAreaHeading)
admin.site.register(CiricLearningAreaSubHeading)
