from django.db import models

class BigHistoryUnit(models.Model):
    unit_number = models.CharField(max_length=50)

    def __str__(self):
        return str(self.id) + ' ' + self.unit_number

class CiricLearningArea(models.Model):
    area_name = models.CharField(max_length=20)
    year = models.IntegerField(default=0)

    def __str__(self):
        return self.area_name + ' year ' + str(self.year) + ' ' + str(self.id)

class CiricLearningAreaHeading(models.Model):
    heading = models.CharField(max_length=200)
    learning_area = models.ForeignKey(CiricLearningArea)
    heading_type = models.CharField(max_length=4, default='SUBJ')

    def __str__(self):
        return self.learning_area.area_name + ': ' + self.heading

class CiricLearningAreaSubHeading(models.Model):
    heading = models.CharField(max_length=200)
    parent_heading = models.ForeignKey(CiricLearningAreaHeading, default=0)
    heading_type = models.CharField(max_length=4, default='CONT')

    def __str__(self):
        return self.parent_heading.learning_area.area_name + ' -> ' + self.parent_heading.heading + ': ' + self.heading

class CiricSpecific(models.Model):
    description = models.TextField(default='')
    parent_heading = models.ForeignKey(CiricLearningAreaSubHeading, default=0)
    code = models.CharField(max_length=20, blank=True, null=True)
    learning_area = models.ForeignKey(CiricLearningArea, default=0)

    def __str__(self):
        return self.parent_heading.heading + ' ' + self.code

    def big_history_units(self):
        return self.mapping_set.values_list('big_history', flat=True)

class Mapping(models.Model):
    big_history = models.ForeignKey(BigHistoryUnit)
    ciric_specific = models.ForeignKey(CiricSpecific)

    def __str__(self):
        return self.big_history.unit_number + ' maps to ' + self.ciric_specific.code
