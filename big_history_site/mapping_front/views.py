from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django_xhtml2pdf.utils import generate_pdf

from .models import BigHistoryUnit, CiricLearningArea, CiricLearningAreaHeading, Mapping, CiricSpecific, CiricLearningAreaSubHeading

def index(request):
    return render(request, 'main_view.html')

def show_bh_unit(request, bh_unit_id, year_id):
    # try to find the bh unit
    try:
        bh_unit = BigHistoryUnit.objects.get(pk=bh_unit_id)
        learning_areas = CiricLearningArea.objects.filter(year=year_id).exclude(area_name="Cross Curriculum").exclude(area_name="General Capabilites")
        mappings = Mapping.objects.filter(big_history_id=bh_unit_id)
    except:
        raise Http404('Big History unit does not exist')

    # show bh circles
    return render(request, 'unit_view.html', { 'bh_unit': bh_unit, 'learning_areas': learning_areas, 'mappings': mappings, 'year': year_id })

def bh_unit_json(request, bh_unit_id, year_id):
    # grab those big history unit mappings
    mappings = Mapping.objects.filter(big_history_id=bh_unit_id)

    # setup placeholders
    return_data = {}

    # loop through mappings
    for mapping in mappings:
        # grab learning area
        learning_area = str(mapping.ciric_specific.learning_area_id)

        # check if we need to add new dict
        if learning_area not in return_data:
            # create new dict with base amounts
            return_data[learning_area] = { 'CONT': 0, 'CPTS': 0, 'ACHT': 0 }

        # grab amount of items
        amount = return_data[learning_area][mapping.ciric_specific.parent_heading.heading_type]

        # increment
        return_data[learning_area][mapping.ciric_specific.parent_heading.heading_type] = amount + 1


    # turn into usable json for d3
    data = { 'name': 'flare', 'color': '#2e2e2e', 'children': [] }

    # add children
    for learning_area in CiricLearningArea.objects.filter(year=year_id).exclude(area_name='Cross Curriculum').exclude(area_name='General Capabilites'):
        # create area to append
        to_append = {
            'name': learning_area.area_name,
            'color': '#474747',
            'link': learning_area.id,
            'children': [
                {
                    'name': 'Achievement',
                    'type': 'ACHT',
                    'color': '#616161',
                    'link': learning_area.id,
                    'children': []
                },
                {
                    'name': 'Content',
                    'type': 'CONT',
                    'color': '#616161',
                    'link': learning_area.id,
                    'children': []
                },
                {
                    'name': 'Concepts',
                    'type': 'CPTS',
                    'color': '#616161',
                    'link': learning_area.id,
                    'children': []
                },
            ]
        }

        # add number of children
        for key, area in enumerate(to_append['children']):
            # check if exists
            if str(learning_area.id) in return_data:
                # work out how many nodes
                nodes = return_data[str(learning_area.id)][area['type']]

                # loop and add nodes
                for i in range(nodes):
                    to_append['children'][key]['children'].append({ 'name': '', 'color': '#7a7a7a' })

        # append
        data['children'].append(to_append)



    # return as json
    return JsonResponse(data, safe=False)

def show_grid_view(request, learning_area_id):
    # try find the learning area and headings to go along with it
    try:
        learning_areas = CiricLearningArea.objects.order_by('id')
        learning_area = CiricLearningArea.objects.get(pk=learning_area_id)
        headings1 = learning_area.ciriclearningareaheading_set.filter(heading='Concepts')
        headings2 = learning_area.ciriclearningareaheading_set.exclude(heading='Concepts')
        headings = headings1 | headings2
    except CiricLearningArea.DoesNotExist:
        raise Http404('Learning area does not exist')

    return render(request, 'grid_view.html', { 'learning_area': learning_area, 'headings': headings,'learning_areas':learning_areas, 'bh': range(1, 11) })

# old views for seperate big hist, curric view
#def big_hist_view(request):
#    return render(request, "big_history_view.html")
#def show_syllabus_view(request):
#    return render(request, 'syllabus_view.html')

def download_grid_view(request, learning_area_id):
    # begin response
    resp = HttpResponse(content_type='application/pdf')

    # create context
    context = {}

    # try find the learning area and headings to go along with it
    try:
        context['learning_area'] = CiricLearningArea.objects.get(pk=learning_area_id)
        context['headings'] = context['learning_area'].ciriclearningareaheading_set.all()
    except CiricLearningArea.DoesNotExist:
        raise Http404('Learning area does not exist')

    # show grid view response
    context['bh'] = range(1, 11)

    # create result
    result = generate_pdf('pdf_grid_view.html', file_object=resp, context=context)

    # return result
    return result
    # return render(request, 'grid_view.html', { 'learning_area': learning_area, 'headings': headings,'learning_areas':learning_areas, 'bh': range(1, 11) })

def show_admin_login(request):
    #check if already logged in
    if request.user.is_authenticated():
        return redirect('show_admin')

    if request.method == 'GET':
        return render(request, "bh_admin/login_view.html")
    elif request.method == 'POST':
        # grab username and password
        username_inp = request.POST.get('username', '')
        password_inp = request.POST.get('password', '')

        # try authenticate
        user = authenticate(username=username_inp, password=password_inp)

        #check if it works, if so then login
        if user is not None:
            login(request, user)

            # take user on
            return redirect('show_admin')
        else:
            return render(request, "bh_admin/login_view.html")

def logout_view(request):
    logout(request)

    # redirect to login page
    return redirect('bh_admin_login')

@login_required(login_url='bh_admin/login')
def show_admin(request):
    # grab learning areas for each year
    nine_areas = CiricLearningArea.objects.filter(year=9).order_by('area_name')
    ten_areas = CiricLearningArea.objects.filter(year=10).order_by('area_name')

    # return template
    return render(request, 'bh_admin/admin_view.html', { 'nine': nine_areas, 'ten': ten_areas })

@login_required(login_url='bh_admin/login')
def add_learning_area(request):
    # grab details from request
    year = request.POST.get('year')
    area = request.POST.get('new_area')

    # create new learning area
    learning_area = CiricLearningArea(year=year, area_name=area)

    # save
    learning_area.save()

    # send back to admin
    return redirect('show_admin')

@login_required(login_url='bh_admin/login')
def edit_learning_area(request, learning_area_id):
    # try find the learning area and headings to go along with it
    try:
        learning_area = CiricLearningArea.objects.get(pk=learning_area_id)
        headings = learning_area.ciriclearningareaheading_set.all()
    except CiricLearningArea.DoesNotExist:
        raise Http404('Learning area does not exist')

    # show grid view response
    return render(request, 'edit_grid_view.html', { 'learning_area': learning_area, 'headings': headings, 'bh': range(1, 11)})

@login_required(login_url='bh_admin/login')
def check_bh(request, ciric_id, bh_id):
    # create new mapping
    mapping = Mapping(big_history_id=bh_id, ciric_specific_id=ciric_id)

    # commit to model by saving
    mapping.save()

    # return added response
    return HttpResponse('added')

@login_required(login_url='bh_admin/login')
def uncheck_bh(request, ciric_id, bh_id):
    # try find the mapping model if it exists
    try:
        # grab model and delete if exits
        mapping = Mapping.objects.get(big_history_id=bh_id, ciric_specific_id=ciric_id)
        mapping.delete()
    except Mapping.DoesNotExist:
        # 404 not found if no model
        raise Http404('Uh oh!')

    # deleted response
    return HttpResponse('deleted')

@login_required(login_url='bh_admin/login')
def delete_learning_area(request, learning_area_id):
    # try find model
    try:
        learning_area = CiricLearningArea.objects.get(id=learning_area_id)
        learning_area.delete()
    except CiricLearningArea.DoesNotExist:
        raise Http404('could not delete learning area')

    # send back to admin
    return redirect('show_admin')

@login_required(login_url='bh_admin/login')
def add_point(request, learning_area_id, parent_heading_id):
    # check for post method
    if request.method == 'POST':
        # grab the variables
        desc = request.POST.get('desc', '')
        code = request.POST.get('code', '')

        # create new cirric specific
        ciric_specific = CiricSpecific(description=desc, code=code, parent_heading_id=parent_heading_id)

        # save the item to db
        ciric_specific.save()

        # redirect
        return redirect('edit_learning_area', learning_area_id=learning_area_id)
    elif request.method == 'GET':
        raise Http404('Uh oh!')

@login_required(login_url='bh_admin/login')
def add_subheading(request, learning_area_id, parent_heading_id):
    # check for post method
    if request.method == 'POST':
        # grab the variables
        sub = request.POST.get('sub_heading', '')

        # create new subheading
        sub_heading = CiricLearningAreaSubHeading(heading=sub, parent_heading_id=parent_heading_id)

        # save the item to db
        sub_heading.save()

        # redirect
        return redirect('edit_learning_area', learning_area_id=learning_area_id)
    elif request.method == 'GET':
        raise Http404('Uh oh!')

@login_required(login_url='bh_admin/login')
def add_heading(request, learning_area_id):
    # check for post method
    if request.method == 'POST':
        # grab the variables
        heading_input = request.POST.get('heading', '')

        # create new subheading
        heading = CiricLearningAreaHeading(heading=heading_input, learning_area_id=learning_area_id)

        # save the item to db
        heading.save()

        # redirect
        return redirect('edit_learning_area', learning_area_id=learning_area_id)
    elif request.method == 'GET':
        raise Http404('Uh oh!')

@login_required(login_url='bh_admin/login')
def update_heading(request, learning_area_id, heading_id):
    # check for post
    if request.method == 'POST':
        # grab heading
        heading_input = request.POST.get('heading', '')

        # fetch heading
        aheading = CiricLearningAreaHeading.objects.get(id=heading_id)

        # update the heading
        aheading.heading = heading_input

        # save heading
        aheading.save()

        # redirect
        return HttpResponse('success')

@login_required(login_url='bh_admin/login')
def update_subheading(request, learning_area_id, subheading_id):
    # check for post
    if request.method == 'POST':
        # grab heading
        heading_input = request.POST.get('heading', '')

        # fetch heading
        aheading = CiricLearningAreaSubHeading.objects.get(id=subheading_id)

        # update the heading
        aheading.heading = heading_input

        # save heading
        aheading.save()

        # redirect
        return HttpResponse('success')

@login_required(login_url='bh_admin/login')
def update_point(request, learning_area_id, point_id):
    # check for post
    if request.method == 'POST':
        # grab input
        point_input = request.POST.get('point', '')
        code_input = request.POST.get('code', '')

        # fetch point
        point = CiricSpecific.objects.get(id=point_id)

        # if not blank
        if point_input != '':
            point.description = point_input

        if code_input != '':
            point.code = code_input

        # save point
        point.save()

        # redirect
        return HttpResponse('success')

# remove cirric specific
def delete_ciric_specific(request, learning_area_id, ciric_specific_id):
    # try find the ciric object
    ciric_specific = get_object_or_404(CiricSpecific, pk=ciric_specific_id)

    # delete the ciric item
    ciric_specific.delete()

    # send back
    return redirect('edit_learning_area', learning_area_id=learning_area_id)

# remove subheading
def delete_ciric_subheading(request, learning_area_id, heading_id):
    # try find the heading
    heading = get_object_or_404(CiricLearningAreaSubHeading, pk=heading_id)

    # delete the heading
    heading.delete()

    # send back
    return redirect('edit_learning_area', learning_area_id=learning_area_id)

# remove subheading
def delete_ciric_heading(request, learning_area_id, heading_id):
    # try find the heading
    heading = get_object_or_404(CiricLearningAreaHeading, pk=heading_id)

    # delete the heading
    heading.delete()

    # send back
    return redirect('edit_learning_area', learning_area_id=learning_area_id)
