from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from . import views

urlpatterns = [
    # base url
    url(r'^$', views.index, name='index'),

    # grid view/via syllabus
    url(r'grid_view/(?P<learning_area_id>[0-9]+)/$', views.show_grid_view, name='show_grid_view'),

    # download grid view
    url(r'grid_view/(?P<learning_area_id>[0-9]+)/download$', views.download_grid_view, name='download_grid_view'),

    # big history view off main page
    # url(r'^big_history_view/$', views.big_hist_view, name='big_hist_view'),

    # show big history view of a subject
    url(r'^big_history_view/bh_unit/(?P<bh_unit_id>[0-9]+)/(?P<year_id>[0-9]+)/$', views.show_bh_unit, name='show_bh_unit'),

    # show syllabus view from main page
    # url(r'^syllabus_view/$', views.show_syllabus_view, name='show_syllabus_view'),

    ## START ADMIN ROUTES ##

    # admin area
    url(r'bh_admin/$', views.show_admin, name='show_admin'),

    # add learning area
    url(r'^bh_admin/add_learning_area', views.add_learning_area, name='add_learning_area'),
    #remove learning area
    url(r'^bh_admin/delete_learning_area/(?P<learning_area_id>[0-9]+)/$', views.delete_learning_area, name='delete_learning_area'),
    # edit learning area
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/$', views.edit_learning_area, name='edit_learning_area'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/(?P<parent_heading_id>[0-9]+)/add_point/$', views.add_point, name='add_point'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/(?P<parent_heading_id>[0-9]+)/add_subheading/$', views.add_subheading, name='add_subheading'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/add_heading/$', views.add_heading, name='add_heading'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/update_heading/(?P<heading_id>[0-9]+)$', views.update_heading, name='update_heading'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/update_subheading/(?P<subheading_id>[0-9]+)$', views.update_subheading, name='update_subheading'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/update_point/(?P<point_id>[0-9]+)$', views.update_point, name='update_point'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/remove_specific/(?P<ciric_specific_id>[0-9]+)/$', views.delete_ciric_specific, name='delete_ciric_specific'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/remove_subheading/(?P<heading_id>[0-9]+)/$', views.delete_ciric_subheading, name='delete_ciric_subheading'),
    url(r'^bh_admin/edit_learning_area/(?P<learning_area_id>[0-9]+)/remove_heading/(?P<heading_id>[0-9]+)/$', views.delete_ciric_heading, name='delete_ciric_heading'),

    # update ciric specific admin
    url(r'^bh_admin/ciric_specific/(?P<ciric_id>[0-9]+)/check/(?P<bh_id>[0-9]+)/$', views.check_bh, name='check_bh'),
    url(r'^bh_admin/ciric_specific/(?P<ciric_id>[0-9]+)/uncheck/(?P<bh_id>[0-9]+)/$', views.uncheck_bh, name='uncheck_bh'),

    # admin login page
    url(r'bh_admin/login', views.show_admin_login, name='bh_admin_login'),

    # admin login page
    url(r'bh_admin/logout', views.logout_view, name='bh_admin_logout'),

    ## END ADMIN ROUTES ##

    # json routes
    url(r'big_history/(?P<bh_unit_id>[0-9]+)/(?P<year_id>[0-9]+).json$', views.bh_unit_json, name='bh_unit_json')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
