# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-14 00:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mapping_front', '0009_ciriclearningareasubheading_heading_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='ciricspecific',
            name='learning_area',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='mapping_front.CiricLearningArea'),
        ),
    ]
